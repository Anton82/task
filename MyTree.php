<?php

/**
 * Created by PhpStorm.
 * User: 1
 * Date: 11.09.2016
 * Time: 18:12
 */
class MyTree extends Tree
{
    private $tree = [];

    public function createNode(Node $node, $parentNode = NULL)
    {
        $parentNode                   = is_null($parentNode) ? 0 : $parentNode;
        $this->tree[$node->getName()] = [
            'node'   => $node,
            'parent' => $parentNode
        ];
    }


    public function deleteNode(Node $node)
    {
        foreach ($this->tree as $k => $item) {
            if ($item['parent'] == $node || $item['node'] == $node) {
                unset($this->tree[$k]);
            }
        }
    }

    public function attachNode(Node $node, Node $parent)
    {
        $nName = $node->getName();
        if (isset($this->tree[$nName])) {
            $this->tree[$nName]['parent'] = $parent;
        }
    }

    public function getNode($nodeName)
    {
        if (isset($this->tree[$nodeName])) {
            return $this->tree[$nodeName]['node'];
        }

        return null;
    }

    public function export()
    {
        $tree = $this->tree;

        return $this->makeTree($tree);
    }

    private function makeTree(&$tree, $pid = 0)
    {
        $res = [];
        foreach ($tree as $n => $item) {
            if ($item['parent'] == $pid) {
                $r = ['name' => $n];
                unset($tree[$n]);
                $children = $this->makeTree($tree, $item['node']);
                if (count($children) > 0) {
                    $r['children'] = $children;
                }
                $res[] = $r;
            }
        }

        return $res;
    }
}